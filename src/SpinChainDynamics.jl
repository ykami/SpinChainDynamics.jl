module SpinChainDynamics

export SpinSystem
export update!
export makeContinuousTimeSampler!, makeSequentialUpdateSampler!
export copy

using LinearAlgebra
using Random, Distributions
using StaticArrays

mutable struct SpinSystem{TValue<:AbstractFloat, TLabel<:Integer}
    configuration::BitVector
    quadratic::SMatrix{4, 4, TValue}
    linear::SMatrix{2, 2, TValue}
    edgeIntensity::TValue
    vertexIntensity::TValue
    biasedEdges::Vector{Pair{TLabel, TLabel}}
    biasedVertices::Vector{TLabel}

    @doc raw"""
        SpinSystem(::BitVector, ::SMatrix{4, 4, <:Number}, ::SMatrix{2, 2, <:Number}; ::Vector{Pair{Any, Any}}, ::Vector{Any})

    Constructor `SpinSystem` with two arguments.
    This struct type normalizes `quadratic` and `linear` matrices by the minimum diagonal component (all diagonal components are negative) in each of these two matrices.

    # Arguments
    - `configuration::BitVector`: an initial spin configuration of which each spin takes binary numbers (boolean values).
    - `quadratic::SMatrix{4, 4, <:Number}`: a left infinitesimal generator with size ``4 \times 4`` acting on two spin states.
    - `linear::SMatrix{2, 2, <:Number}`: a left infinitesimal generator with size ``2 \times 2`` acting on one spin state.
    - `biasedEdges::Vector{Pair{Any, Any}}=[]`: a list of edges on which `quadratic` acts.
    - `biasedVertices::Vector{Any}=[]`: a list of vertices on which `quadratic` acts.

    The parameter `quadratic` and `linear` must satisfy the conditions for [transition-rate matrices](https://en.wikipedia.org/wiki/Transition-rate_matrix).
    Let ``Q = [q_{i, j}]_{i, j = 1}^{n}`` be a ``n \times n`` matrix.
    We say that ``Q`` is a left (right) transition-rate matrix if

    ```math
        ∀ i, j \left[i, j ∈ \{1, …, n\} ∧ i ≠ j ⟹ q_{i, j} ≥ 0\right],
    ```

    and

    ```math
    \begin{aligned}
        &∀ j \left[j ∈ \{1, …, n\} ⟹ q_{j, j} = -\sum_{\substack{i=1\\ (i ≠ j)}}^{n} q_{i, j}\right] \\
        &\left(∀ i \left[i ∈ \{1, …, n\} ⟹ q_{i, i} = -\sum_{\substack{j=1\\ (j ≠ i)}}^{n} q_{i, j}\right]\right).
    \end{aligned}
    ```

    Note that, by putting ``λ = -\min\{q_{i, i} \mid i ∈ \{1, …, n\}\}``, the matrix ``P = I + Q / λ`` is a left (right) [stochastic matrix](https://en.wikipedia.org/wiki/Stochastic_matrix), where ``I`` is the ``n \times n`` unit matrix.
    """
    function SpinSystem(configuration::AbstractVector{Bool}, quadratic::AbstractMatrix{<:Number}, linear::AbstractMatrix{<:Number}; biasedEdges::AbstractVector{Pair{TLabel, TLabel}}=empty([], Pair{Int, Int}), biasedVertices::AbstractVector{TLabel}=empty([], Int), dtype::DataType=Float64) where {TLabel<:Integer}
        # Check definition of transition-rate matrices.
        (row, column) = size(quadratic)
        if row != 4 || column != 4
            throw(DimensionMismatch("The quadratic generator does not have the correct size: $(row)rows ≠ 4 or $(column)columns ≠ 4."))
        end
        if any(diag(quadratic) .> 0.0)
            throw(DomainError(quadratic[findfirst(x -> x > 0.0, quadratic)], "The diagonal components must be negative."))
        elseif any(quadratic - Diagonal(quadratic) .< 0.0)
            throw(DomainError(quadratic[findfirst(x -> x < 0.0, quadratic - Diagonal(quadratic))], "The non-diagonal components must be positive."))
        elseif !all(isapprox.(sum(quadratic, dims=1), 0.0; atol=eps(Float64)))
            throw(ArgumentError("The sum for each column vector must be zero."))
        end
        (row, column) = size(linear)
        if row != 2 || column != 2
            throw(DimensionMismatch("The linear generator does not have the correct size: $(row)rows ≠ 2 or $(column)columns ≠ 2."))
        end
        if any(diag(linear) .> 0.0)
            throw(DomainError(linear[findfirst(x -> x > 0.0, linear)], "The diagonal components must be negative."))
        elseif any(linear - Diagonal(linear) .< 0.0)
            throw(DomainError(linear[findfirst(x -> x < 0.0, linear - Diagonal(linear))], "The non-diagonal components must be positive."))
        elseif !all(isapprox.(sum(linear, dims=1), 0.0; atol=eps(Float64)))
            throw(ArgumentError("The sum for each column vector must be zero."))
        end

        # Normalization.
        edgeIntensity = maximum(-diag(quadratic))
        if !iszero(edgeIntensity)
            quadratic /= edgeIntensity
        end
        vertexIntensity = maximum(-diag(linear))
        if !iszero(vertexIntensity)
            linear /= vertexIntensity
        end

        # TODO: Check errors for biased edges and vertices.
        return new{dtype, TLabel}(configuration, SMatrix{4, 4}(float.(quadratic)), SMatrix{2, 2}(float.(linear)), float(edgeIntensity), float(vertexIntensity), biasedEdges, biasedVertices)
    end
end

# Getter/Setter
configuration(system::SpinSystem)::BitVector = system.configuration
configuration(system::SpinSystem, configuration::BitVector) = (system.configuration = configuration)
quadratic(system::SpinSystem)::SMatrix{4, 4, TValue} where {TValue<:AbstractFloat} = system.quadratic
linear(system::SpinSystem)::SMatrix{2,2,TValue} where {TValue<:AbstractFloat} = system.linear
edgeIntensity(system::SpinSystem)::TValue where {TValue<:AbstractFloat} = system.edgeIntensity
vertexIntensity(system::SpinSystem)::TValue where {TValue<:AbstractFloat} = system.vertexIntensity

# Copy constructor
function Base.copy(system::SpinSystem{TValue,TLabel})::SpinSystem{TValue,TLabel} where {TValue<:AbstractFloat, TLabel<:Integer}
    return SpinSystem(
        copy(system.configuration),
        system.quadratic,
        system.linear;
        biasedEdges=system.biasedEdges,
        biasedVertices=system.biasedVertices,
    )
end

function update!(configuration::BitVector, quadratic::SMatrix{4, 4, TValue}, updatedEdge::Pair{TLabel, TLabel}, u::TValue) where {TValue<:AbstractFloat, TLabel<:Integer}
    j = 2 * configuration[updatedEdge.first] + 1 * configuration[updatedEdge.second] + 1  # The column index = a binary "configuration[updatedEdge.first] configuration[updatedEdge.second]" converted into the digit and added 1 to it.
    i = argmax(cumsum((I + quadratic)[:, j], dims=1) .> u)  # The row index whose the first element exceeds u.
    configuration[updatedEdge.first] = (i - 1) ÷ 2
    configuration[updatedEdge.second] = (i - 1) % 2
end

function update(configuration::BitVector, quadratic::SMatrix{4, 4, TValue}, updatedEdge::Pair{TLabel, TLabel}, u::TValue)::BitVector where {TValue<:AbstractFloat, TLabel<:Integer}
    j = 2 * configuration[updatedEdge.first] + 1 * configuration[updatedEdge.second] + 1  # The column index = a binary "configuration[updatedEdge.first] configuration[updatedEdge.second]" converted into the digit and added 1 to it.
    i = argmax(cumsum((I + quadratic)[:, j], dims=1) .> u)  # The row index whose the first element exceeds u.
    newConfiguration = copy(configuration)
    newConfiguration[updatedEdge.first] = (i - 1) ÷ 2
    newConfiguration[updatedEdge.second] = (i - 1) % 2
    return newConfiguration
end

function update!(configuration::BitVector, linear::SMatrix{2, 2, TValue}, updatedVertex::TLabel, u::TValue) where {TValue<:AbstractFloat, TLabel<:Integer}
    j = 1 * configuration[updatedVertex] + 1  # The column index = a binary "configuration[updatedVertex]" converted into the digit and added 1 to it.
    i = argmax(cumsum((I + linear)[:, j], dims=1) .> u)  # The row index whose the first element exceeds u.
    configuration[updatedVertex] = i - 1
end

function update(configuration::BitVector, linear::SMatrix{2, 2, TValue}, updatedVertex::TLabel, u::TValue)::BitVector where {TValue<:AbstractFloat, TLabel<:Integer}
    j = 1 * configuration[updatedVertex] + 1  # The column index = a binary "configuration[updatedVertex]" converted into the digit and added 1 to it.
    i = argmax(cumsum((I + linear)[:, j], dims=1) .> u)  # The row index whose the first element exceeds u.
    newConfiguration = copy(configuration)
    newConfiguration[updatedVertex] = i - 1
    return newConfiguration
end

@doc """
    update!(::SpinSystem, ::Pair{Any, Any}, ::AbstractFloat; ::AbstractFloat)

Updating `SpinSystem.configuration` by the MCMC based on `SpinSystem.quadratic`.

# Arguments
- `system::SpinSystem`: a target spin system.
- `updatedEdge::Pair{Any, Any}`: an edge on which the stochastic matrix `I + SpinSystem.quadratic` acts.
- `u::AbstractFloat`: an uniform random number.
- `intensity::AbstractFloat=min(1.0, system.edgeIntensity)`: a scaling factor for the left infinitesimal generator `system.quadratic`.
"""
function update!(system::SpinSystem{TValue, TLabel}, updatedEdge::Pair{TLabel, TLabel}, u::TValue; intensity::TValue=min(1.0, system.edgeIntensity)) where {TValue<:AbstractFloat, TLabel<:Integer}
    update!(system.configuration, intensity * system.quadratic, updatedEdge, u)
end

@doc """
    update!(::SpinSystem, ::Any, ::AbstractFloat; ::AbstractFloat)

Updating `SpinSystem.configuration` by the MCMC based on `SpinSystem.linear`.

# Arguments
- `system::SpinSystem`: a target spin system.
- `updatedVertex::Any`: a vertex on which the stochastic matrix `I + SpinSystem.linear` acts.
- `u::AbstractFloat`: an uniform random number.
- `intensity::AbstractFloat=min(1.0, system.vertexIntensity)`: a scaling factor for the left infinitesimal generator `system.linear`.
"""
function update!(system::SpinSystem{TValue, TLabel}, updatedVertex::TLabel, u::TValue; intensity::TValue=min(1.0, system.vertexIntensity)) where {TValue<:AbstractFloat, TLabel<:Integer}
    update!(system.configuration, intensity * system.linear, updatedVertex, u)
end

@doc """
    update!(::SpinSystem; ::AbstractRNG)

Updating `SpinSystem.configuration` by the MCMC based on `SpinSystem.quadratic` and `SpinSystem.linear`.
These infinitesimal generators are randomly chosen from the Bernoulli distribution with the parameter depending on the ratio between the number of edges and vertices.
This method should be used in the discrete time setting.

# Arguments
- `system::SpinSystem`: a target spin system.
- `rng::AbstractRNG=Random.default_rng()`: a random number generator for uniform random variables to update `system`.
"""
function update!(system::SpinSystem; rng::AbstractRNG=Random.default_rng())
    normalizationConstant = max(system.edgeIntensity, system.vertexIntensity)
    numEdges = length(system.biasedEdges)
    numVertices = length(system.biasedVertices)
    u = rand(rng, Uniform())
    if rand(rng, Bernoulli(numEdges / (numEdges + numVertices)))
        updatedEdge = rand(rng, system.biasedEdges)
        update!(system.configuration, system.edgeIntensity / normalizationConstant * system.quadratic, updatedEdge, u)
    else
        updatedVertex = rand(rng, system.biasedVertices)
        update!(system.configuration, system.vertexIntensity / normalizationConstant * system.linear, updatedVertex, u)
    end
end

@doc """
    makeContinuousTimeSampler!(::SpinSystem, ::AbstractFloat; ::AbstractRNG)

Taking samples from a [continuous-time Markov chain](https://en.wikipedia.org/wiki/Continuous-time_Markov_chain) defined by `system`.
Its transition probability is governed by a [master equation](https://en.wikipedia.org/wiki/Master_equation), which is also called [Kolmogorov forward/backward equations](https://en.wikipedia.org/wiki/Kolmogorov_equations).
An initial spin configuration of `system` is always taken as the first sample at time zero.

# Arguments
- `system::SpinSystem`: a target spin system.
- `finalTime::AbstractFloat`: a final time of a time interval to take samples.
- `rng::AbstractRNG=Random.default_rng()`: a random number generator to take time series obaying Piosson clocks and to update `system`.
"""
function makeContinuousTimeSampler!(system::SpinSystem{TValue, TLabel}, finalTime::TTime; rng::TRNG=Random.default_rng())::Channel{Tuple{TTime, BitVector}} where {TValue<:AbstractFloat, TLabel<:Integer, TTime<:AbstractFloat, TRNG<:AbstractRNG}
    timelineForUpdatedEdges = (collect ∘ Iterators.flatten ∘ map)(
        pair -> zip(rand(rng, Uniform(0.0, finalTime), pair.second),
                    fill(pair.first, pair.second),
                    rand(rng, Uniform(), pair.second)),
        system.biasedEdges .=> rand(rng, Poisson(system.edgeIntensity * finalTime), length(system.biasedEdges))
    )
    timelineForUpdatedVertices = (collect ∘ Iterators.flatten ∘ map)(
        pair -> zip(rand(rng, Uniform(0.0, finalTime), pair.second),
                    fill(pair.first, pair.second),
                    rand(rng, Uniform(), pair.second)),
        system.biasedVertices .=> rand(rng, Poisson(system.vertexIntensity * finalTime), length(system.biasedVertices))
    )
    timeline = sort(vcat(timelineForUpdatedEdges, timelineForUpdatedVertices), by=item -> item[1])

    Channel{Tuple{TTime, BitVector}}() do channel
        put!(channel, (0.0, copy(system.configuration)))
        for (time, updatedTarget, u) in timeline
            update!(system, updatedTarget, u; intensity=1.0)
            put!(channel, (time, copy(system.configuration)))
        end
    end
end

@doc """
    makeContinuousTimeSampler!(::SpinSystem, ::AbstractFloat, ::Integer; ::AbstractRNG)

Taking `numSamples` samples from a continuous-time Markov chain defined by `system`.
This is a wrapper method for [`makeContinuousTimeSampler!(::SpinSystem, ::AbstractFloat; ::AbstractRNG)`](@ref).
An initial spin configuration of `system` is always taken as the first sample at time zero.
The parameter `numSamples` means the number of samples excluding the first one.
Due to the specification for the `range` function, one must pass a integer number greater than or equal to `1` (the total number of samples is greater than or equal to `2`) as the `numSamples` argument.

# Arguments
- `system::SpinSystem`: a target spin system.
- `finalTime::AbstractFloat`: a final time of a time interval to take samples.
- `numSamples::Integer`: the number of samples after time zero.
- `rng::AbstractRNG=Random.default_rng()`: a random number generator to take time series obaying Piosson clocks and to update `system`.
"""
function makeContinuousTimeSampler!(system::SpinSystem{TValue, TLabel}, finalTime::TTime, numSamples::TCardinality; rng::TRNG=Random.default_rng())::Channel{Tuple{TTime, BitVector}} where {TValue<:AbstractFloat, TLabel<:Integer, TTime<:AbstractFloat, TCardinality<:Integer, TRNG<:AbstractRNG}
    sampler = makeContinuousTimeSampler!(system, finalTime; rng=rng)

    Channel{Tuple{TTime, BitVector}}() do channel
        current = iterate(sampler)
        next = iterate(sampler, current[2])
        for time in range(0.0, finalTime, length=numSamples + 1)  # A sample at the initial time 0 is always taken.
            while (next !== nothing) && (time > next[1][1])
                current = next
                next = iterate(sampler, current[2])
            end
            put!(channel, (time, current[1][2]))
        end
    end
end

@doc """
    makeSequentialUpdateSampler!(::SpinSystem, ::Integer; ::AbstractRNG, ::Bool)

Taking `numSamples` samples from a discrete-time Markov chain defined by `system`.
Their transition probability matrices are given by `LinearAlgebra.I + system.edgeIntensity * system.quadratic` and `LinearAlgebra.I + system.vertexIntensity * system.linear`.
Updated edges and vertices are chosen sequentially.
The order follows the forward direction of the indices of `system.biasedEdges` and `system.biasedVertices`.
If one pass the `true` value as the `rev` argument, then the order follows the backward direction of the indices.
This method first updates targets (edges/vertices) whose intensity (`edgeIntensity`/`vertexIntensity`) is bigger, and then this updates the other targes.
The process choosing the edge set and the vertex set is continued alternately.
Time increases by `1` after all elements of the edge set and the vertex set are sweeped.
Note that the time variable is different from the Monte Carlo step.

# Arguments
- `system::SpinSystem`: a target spin system.
- `finalTime::Integer`: a final time until which the MCMC simulation stops.
- `rng::AbstractRNG=Random.default_rng()`: a random number generator to update `system`.
- `rev::Bool=false`: this functions reverts to the order to choose updated targets if `rev` is `true`.
"""
function makeSequentialUpdateSampler!(system::SpinSystem{TValue, TLabel}, finalTime::TTime; rng::TRNG=Random.default_rng(), rev::Bool=false)::Channel{Tuple{TTime, BitVector}} where {TValue<:AbstractFloat, TLabel<:Integer, TTime<:Integer, TRNG<:AbstractRNG}
    timelineForUpdatedEdges = (collect ∘ zip)(
        repeat(1:finalTime, inner=length(system.biasedEdges)),
        repeat(sort(system.biasedEdges, rev=rev), finalTime),
        rand(rng, Uniform(), finalTime * length(system.biasedEdges))
    )
    timelineForUpdatedVertices = (collect ∘ zip)(
        repeat(1:finalTime, inner=length(system.biasedVertices)),
        repeat(sort(system.biasedVertices, rev=rev), finalTime),
        rand(rng, Uniform(), finalTime * length(system.biasedVertices))
    )
    timeline = sort(
        ifelse(
            system.edgeIntensity >= system.vertexIntensity,
            vcat(timelineForUpdatedEdges, timelineForUpdatedVertices),
            vcat(timelineForUpdatedVertices, timelineForUpdatedEdges)
        ),
        by=item -> item[1]
    )

    Channel{Tuple{TTime, BitVector}}() do channel
        put!(channel, (0, copy(system.configuration)))
        for (time, updatedTarget, u) in timeline
            update!(system, updatedTarget, u)
            put!(channel, (time, copy(system.configuration)))
        end
    end
end

@doc """
    makeSequentialUpdateSampler!(::SpinSystem, ::Integer, ::Integer; ::AbstractRNG, ::Bool)

Taking samples from a [discrete-time Markov chain](https://en.wikipedia.org/wiki/Discrete-time_Markov_chain) defined by `system`.
This is a wrapper method for [`makeSequentialUpdateSampler!(::SpinSystem, ::Integer; ::AbstractRNG, ::Bool)`](@ref).

# Arguments
- `system::SpinSystem`: a target spin system.
- `finalTime::Integer`: a final time until which the MCMC simulation stops.
- `numSamples::Integer`: the number of samples after time zero.
- `rng::AbstractRNG=Random.default_rng()`: a random number generator to update `system`.
- `rev::Bool=false`: this functions reverts to the order to choose updated targets if `rev` is `true`.
"""
function makeSequentialUpdateSampler!(system::SpinSystem{TValue, TLabel}, finalTime::TTime, numSamples::TCardinality; rng::TRNG=Random.default_rng(), rev::Bool=false)::Channel{Tuple{TTime, BitVector}} where {TValue<:AbstractFloat, TLabel<:Integer, TTime<:Integer, TCardinality<:Integer, TRNG<:AbstractRNG}
    sampler = makeSequentialUpdateSampler!(system, finalTime; rng=rng, rev=rev)

    Channel{Tuple{TTime, BitVector}}() do channel
        current = iterate(sampler)
        next = iterate(sampler, current[2])
        for time in ceil.(Int, range(0, finalTime, length=numSamples + 1))  # A sample at the initial time 0 is always taken.
            while (next !== nothing) && (time >= next[1][1])
                current = next
                next = iterate(sampler, current[2])
            end
            put!(channel, current[1])
        end
    end
end

end
