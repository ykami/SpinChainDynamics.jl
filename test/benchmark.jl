using BenchmarkTools
import Graphs
using Random
using SpinChainDynamics

function main()
    n = 10
    G = Graphs.cycle_digraph(n)
    biasedEdges = [Graphs.src(e) => Graphs.dst(e) for e in collect(Graphs.edges(G))]
    biasedVertices = collect(Graphs.vertices(G))
    system = SpinSystem(
        falses(n),
        [
            0  0  0 0
            0 -1  1 0
            0  1 -1 0
            0  0  0 0
        ],
        [
            -0.5  0.5
            0.5 -0.5
        ];
        biasedEdges=biasedEdges,
        biasedVertices=biasedVertices,
    )
    for i in 1:10
        rng = Random.seed!(i)
        sampler = makeContinuousTimeSampler!(copy(system), 10.0; rng=rng)
        realization = map(item -> item[2], sampler)
    end
end

@benchmark main()
