# SpinChainDynamics

[![Build Status](https://gitlab.com/ykami/SpinChainDynamics.jl/badges/main/pipeline.svg)](https://gitlab.com/ykami/SpinChainDynamics.jl/pipelines)
[![Coverage](https://gitlab.com/ykami/SpinChainDynamics.jl/badges/main/coverage.svg)](https://gitlab.com/ykami/SpinChainDynamics.jl/commits/main)
[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://ykami.gitlab.io/SpinChainDynamics.jl/dev)
[![Coverage](https://codecov.io/gl/ykami/SpinChainDynamics.jl/branch/main/graph/badge.svg)](https://codecov.io/gl/ykami/SpinChainDynamics.jl)
