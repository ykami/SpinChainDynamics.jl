using SpinChainDynamics
using Documenter

DocMeta.setdocmeta!(SpinChainDynamics, :DocTestSetup, :(using SpinChainDynamics); recursive=true)

makedocs(;
    modules=[SpinChainDynamics],
    authors="Yoshinori Kamijima <8644914-ykami@users.noreply.gitlab.com>",
    repo="https://gitlab.com/ykami/SpinChainDynamics.jl/blob/{commit}{path}#{line}",
    sitename="SpinChainDynamics.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ykami.gitlab.io/SpinChainDynamics.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
