import Graphs
using Random
using SpinChainDynamics
using Test

@testset "Constructor and updating algorithm" begin
    @test_throws DimensionMismatch SpinSystem(
        [false, true], [0 0 0 0; 0 -1 1 0; 0 1 -1 0], [0 0; 0 0]
    )
    @test_throws DomainError SpinSystem(
        [false, true], [2 0 0 0; 0 -1 1 0; 0 1 -1 0; 0 0 0 3], [0 0; 0 0]
    )
    @test_throws DomainError SpinSystem(
        [false, true], [0 -2 0 0; 0 -1 1 0; 0 1 -1 0; 0 0 -3 0], [0 0; 0 0]
    )
    @test_throws ArgumentError SpinSystem(
        [false, true], [0 2 0 3; 0 -1 1 0; 0 1 -1 0; 0 0 0 -3], [0 0; 0 0]
    )
    @test_throws DimensionMismatch SpinSystem(
        [false, true], [0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 0 0], [0 0]
    )
    @test_throws DomainError SpinSystem(
        [false, true], [0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 0 0], [1 0; 0 0]
    )
    @test_throws DomainError SpinSystem(
        [false, true], [0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 0 0], [0 0; -1 0]
    )
    @test_throws ArgumentError SpinSystem(
        [false, true], [0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 0 0], [-1 2; 1 -1]
    )
    system = SpinSystem(
        [false, false],
        [-6 0 0 4; 6 -1 2 0; 0 1 -10 0; 0 0 8 -4],
        [-2 1; 2 -1];
        biasedEdges=[1 => 2],
        biasedVertices=[1, 2],
    )
    @test system.quadratic ==
        [-0.6 0.0 0.0 0.4; 0.6 -0.1 0.2 0.0; 0.0 0.1 -1.0 0.0; 0.0 0.0 0.8 -0.4]
    @test system.linear == [-1.0 0.5; 1.0 -0.5]
    @test system.edgeIntensity == 10.0
    @test system.vertexIntensity == 2.0
    system = SpinSystem(
        [false, false],
        [
            -6 0 0 4
            6 -1 2 0
            0 1 -10 0
            0 0 8 -4
        ],
        [
            -2 1
            2 -1
        ];
        biasedEdges=[1 => 2, 2 => 1],
        biasedVertices=[1, 2],
    )
    update!(system, 1 => 2, 0.5)
    @test system.configuration == [false, true]
    update!(system, 1, 0.5)
    @test system.configuration == [true, true]
    update!(system, 2 => 1, 0.5)
    @test system.configuration == [true, true]
end

@testset "Sampling flow" begin
    n = 10
    #biasedEdges = [mod(i, n) + 1 => mod(i + 1, n) + 1 for i = 0:(n - 1)]  # A periodic path graph.
    #biasedVertices = collect(1:n)
    G = Graphs.cycle_digraph(n)
    biasedEdges = [Graphs.src(e) => Graphs.dst(e) for e in collect(Graphs.edges(G))]
    biasedVertices = collect(Graphs.vertices(G))
    system = SpinSystem(
        falses(n),
        [
            0  0  0 0
            0 -1  1 0
            0  1 -1 0
            0  0  0 0
        ],
        [
            -0.5  0.5
             0.5 -0.5
        ];
        biasedEdges,
        biasedVertices,
    )
    rng = Random.seed!(1024)
    sampler = makeContinuousTimeSampler!(deepcopy(system), 10.0; rng=rng)
    realization = map(item -> item[2], sampler)
    @test all(i -> i in [+1, 0, -1], Iterators.flatten(diff(realization; dims=1)))
    sampler = makeContinuousTimeSampler!(deepcopy(system), 10.0, 20; rng=rng)
    realization = collect(sampler)
    @test length(realization) == 21
    system = SpinSystem(
        [i <= n ÷ 2 for i in 1:n],
        [
            0 0    0 0
            0 0  0.5 0
            0 0 -0.5 0
            0 0    0 0
        ],
        zeros(2, 2);
        biasedEdges=biasedEdges,
    )
    sampler = makeSequentialUpdateSampler!(deepcopy(system), 10; rng=rng)
    realization = map(item -> item[2], sampler)
    @test all(i -> i in [+1, 0, -1], Iterators.flatten(diff(realization; dims=1)))
    sampler = makeSequentialUpdateSampler!(deepcopy(system), 10; rng=rng, rev=true)
    realization = map(item -> item[2], sampler)
    @test all(i -> i in [+1, 0, -1], Iterators.flatten(diff(realization; dims=1)))
    sampler = makeSequentialUpdateSampler!(deepcopy(system), 10, 5; rng=rng)
    realization = collect(sampler)
    @test length(realization) == 6
    sampler = makeSequentialUpdateSampler!(deepcopy(system), 10, 5; rng=rng, rev=true)
    realization = collect(sampler)
    @test length(realization) == 6
end
